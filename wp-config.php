<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'peilon');

/** MySQL database username */
define('DB_USER', 'root');
//define('DB_USER', 'peilon');

/** MySQL database password */
define('DB_PASSWORD', 'root');
//define('DB_PASSWORD', 'peilon1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';Z $@p!&?7n.;mWD=}xg/+y];MBLvATgMHs^lRBMi8W%*z%%b5LEx[]WokF3Q1OE');
define('SECURE_AUTH_KEY',  '87&1}IcB|B.p^(wCEXw[}{Zr|RJlFZM68&HuFv<W]CqLoQ)n.z};-K|P&ODIF69%');
define('LOGGED_IN_KEY',    'o%TmC81mO+][d_/`8*)#?>65}mcnLt><aeG-J:9WaHj$fzRqR?8-aJ i!V?< ROQ');
define('NONCE_KEY',        'inHuC3kaPglzDV4z},qXQ>7?e2^{veSEhlD}|*]g@$OzN)F2Fqcq1m.p+5PKUkvg');
define('AUTH_SALT',        '!c3(+`TJr%|#o/N#tmt:olV? $/^i6!+8|[Kf|2IVsrShvr8s#X$rK<G+(R8|iI0');
define('SECURE_AUTH_SALT', 'Nlt28Axy>G4,o+W!y-e-Pug9|`Dn(JS04|l`h7(&$Xu!gsuieNj9e|_p@skci/-p');
define('LOGGED_IN_SALT',   'Ka[-mO)dHe49W?R@[(9mT7Chy|Fu[owGPpptH@,+0D_|4 HA.w%0N7tSgj+;1+<W');
define('NONCE_SALT',       '=v;^wKudi}*o}.Wxy=~{=6O%`0/h5asm-J/Z]t0!1*:CM@w-K,j{<n4^IbVzsi7}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WP_ALLOW_REPAIR', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
