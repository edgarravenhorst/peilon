msgid ""
msgstr "MIME-Version: 1.0\n"

#. Name.
msgid "Reorder Posts"
msgstr ""

#. Short description.
msgid "A simple and easy way to reorder your custom post types in WordPress."
msgstr ""

#. Screenshot description.
msgid "Reorder Posts allows you to easily drag and drop posts to change their order"
msgstr ""

#. Screenshot description.
msgid "Admin panel settings"
msgstr ""

#. Found in description header.
msgid "Features"
msgstr ""

#. Found in description header.
msgid "Spread the Word"
msgstr ""

#. Found in description header.
msgid "Translations"
msgstr ""

#. Found in description header.
msgid "Support"
msgstr ""

#. Found in description header.
msgid "Credits"
msgstr ""

#. Found in description list item.
msgid "Adds \"Reorder\" sub-menu to all post types by default"
msgstr ""

#. Found in description list item.
msgid "Hierarchical post type support (i.e., supports nested posts)"
msgstr ""

#. Found in description list item.
msgid "Allows you to re-nest hierarchical posts"
msgstr ""

#. Found in description list item.
msgid "Auto-saves order without having to click an update button"
msgstr ""

#. Found in description list item.
msgid "Dedicated settings panel for determining which post types can be reordered"
msgstr ""

#. Found in description list item.
msgid "Advanced settings panel for overriding the menu order of custom post type queries"
msgstr ""

#. Found in description list item.
msgid "German"
msgstr ""

#. Found in description paragraph.
msgid "A simple and easy way to reorder your custom post-type posts in WordPress. Adds drag and drop functionality for post ordering in the WordPress admin panel. Works with custom post-types and regular posts."
msgstr ""

#. Found in description paragraph.
msgid "We consider Reorder Posts a <strong>developer tool</strong>. If you do not know what <code>menu_order</code> or custom queries are, then this plugin is likely not for you."
msgstr ""

#. Found in description paragraph.
msgid "If you like this plugin, please help spread the word.  Rate the plugin.  Write about the plugin.  Something :)"
msgstr ""

#. Found in description paragraph.
msgid "If you would like to contribute a translation, please leave a support request with a link to your translation."
msgstr ""

#. Found in description paragraph.
msgid "You are welcome to help us out and <a href=\"https://github.com/ronalfy/reorder-posts\">contribute on GitHub</a>."
msgstr ""

#. Found in description paragraph.
msgid "Please feel free to leave a support request here or create an <a href=\"https://github.com/ronalfy/reorder-posts/issues\">issue on GitHub</a>.  If you require immediate feedback, feel free to @reply us on Twitter with your support link:  (<a href=\"https://twitter.com/ryanhellyer\">@ryanhellyer</a> or <a href=\"https://twitter.com/ronalfy\">@ronalfy</a>).  Support is always free unless you require some advanced customization out of the scope of the plugin's existing features.  We'll do our best to get with you when we can.  Please rate/review the plugin if we have helped you to show thanks for the support."
msgstr ""

#. Found in description paragraph.
msgid "This plugin was originally developed for <a href=\"https://metronet.no/\">Metronet AS in Norway</a>."
msgstr ""

#. Found in description paragraph.
msgid "The plugin is now independently developed by <a href=\"https://geek.hellyer.kiwi/\">Ryan Hellyer</a>, <a href=\"http://www.ronalfy.com\">Ronald Huereca</a> and <a href=\"http://scottbasgaard.com/\">Scott Basgaard</a>."
msgstr ""

#. Found in description paragraph.
msgid "Banner image courtesy of <a href=\"https://www.flickr.com/photos/pagedooley\">Kevin Dooley</a>."
msgstr ""

#. Found in installation list item.
msgid "Upload <code>metronet-reorder-posts</code> to the <code>/wp-content/plugins/</code> directory."
msgstr ""

#. Found in installation list item.
msgid "Activate the plugin through the 'Plugins' menu in WordPress."
msgstr ""

#. Found in installation paragraph.
msgid "Either install the plugin via the WordPress admin panel, or ..."
msgstr ""

#. Found in installation paragraph.
msgid "For each post type, you will see a new \"Reorder\" submenu.  Simply navigate to \"Reorder\" to change the order of your post types. Changes are saved immediately, there is no need to click a save or update button."
msgstr ""

#. Found in installation paragraph.
msgid "By default, ordering is enabled for all post types.  A settings panel is available for determining which post types to enable ordering for."
msgstr ""

#. Found in installation paragraph.
msgid "Advanced customization is allowed via hooks.  See the <a href=\"https://github.com/ronalfy/reorder-posts#plugin-filters\">Plugin Filters on GitHub</a>."
msgstr ""

#. Found in installation paragraph.
msgid "This tool allows you to easily reorder post types in the back-end of WordPress. How the posts are sorted in the front-end is entirely up to you, as it should be."
msgstr ""

#. Found in installation paragraph.
msgid "We do have advanced settings under <code>Settings-&gt;Reorder Posts</code>, but these should only be used for testing purposes."
msgstr ""

#. Found in installation paragraph.
msgid "You'll want to make use of <a href=\"http://codex.wordpress.org/Class_Reference/WP_Query\">WP_Query</a>, <a href=\"http://codex.wordpress.org/Template_Tags/get_posts\">get_posts</a>, or <a href=\"http://codex.wordpress.org/Plugin_API/Action_Reference/pre_get_posts\">pre_get_posts</a> to modify query behavior on the front-end of your site."
msgstr ""

#. Found in installation paragraph.
msgid "Examples of each are on the respective pages above.  You are welcome to leave a support request if you need help with a query and we'll do our best to get back with you."
msgstr ""

#. Found in changelog list item.
msgid "Released 2015-12-04"
msgstr ""

#. Found in changelog list item.
msgid "Fixed loading animation that displays out of nowhere"
msgstr ""

#. Found in changelog list item.
msgid "Released 2015-11-09"
msgstr ""

#. Found in changelog list item.
msgid "Fixed pagination issue"
msgstr ""

#. Found in changelog list item.
msgid "Released 2015-11-1"
msgstr ""

#. Found in changelog list item.
msgid "Loading animation now shows inline"
msgstr ""

#. Found in changelog list item.
msgid "Released 2015-10-02"
msgstr ""

#. Found in changelog list item.
msgid "Fixing paging offset error in the backend."
msgstr ""

#. Found in changelog list item.
msgid "Updated 2015-08-20 for WordPress 4.3"
msgstr ""

#. Found in changelog list item.
msgid "Released 2015-04-24"
msgstr ""

#. Found in changelog list item.
msgid "Added cache-busting when re-ordering"
msgstr ""

#. Found in changelog list item.
msgid "Added German translation"
msgstr ""

#. Found in changelog list item.
msgid "Ensuring WordPress 4.2 compatibility"
msgstr ""

#. Found in changelog list item.
msgid "Released 2015-01-28"
msgstr ""

#. Found in changelog list item.
msgid "Removed developer notice from Reorder pages"
msgstr ""

#. Found in changelog list item.
msgid "Released 2015-01-21"
msgstr ""

#. Found in changelog list item.
msgid "Improved Reorder save query performance significantly"
msgstr ""

#. Found in changelog list item.
msgid "Released 2015-01-19"
msgstr ""

#. Found in changelog list item.
msgid "Added add-on support"
msgstr ""

#. Found in changelog list item.
msgid "Make sure reordering can only be done by those with edit_pages privileges"
msgstr ""

#. Found in changelog list item.
msgid "Added pagination for performance reasons"
msgstr ""

#. Found in changelog list item.
msgid "Optimized queries for performance reasons"
msgstr ""

#. Found in changelog list item.
msgid "Added warning message for those with a lot of posts"
msgstr ""

#. Found in changelog list item.
msgid "Released 2014-12-26"
msgstr ""

#. Found in changelog list item.
msgid "Bug fix:  Saving admin panel settings resulted in a variety of PHP offset error messages."
msgstr ""

#. Found in changelog list item.
msgid "Bug fix:  Querying multiple post types resulted in PHP illegal offset error messages."
msgstr ""

#. Found in changelog list item.
msgid "Released 2014-12-23"
msgstr ""

#. Found in changelog list item.
msgid "Altered contributor documentation."
msgstr ""

#. Found in changelog list item.
msgid "Adding filters for determining where the Reorder sub-menu will show up."
msgstr ""

#. Found in changelog list item.
msgid "Sub-menu headings now reflect the post type that is being re-ordered."
msgstr ""

#. Found in changelog list item.
msgid "Fixed bug in display when there are no post types to re-order."
msgstr ""

#. Found in changelog list item.
msgid "Changed class names to be more unique."
msgstr ""

#. Found in changelog list item.
msgid "Released 2014-12-12 "
msgstr ""

#. Found in changelog list item.
msgid "Added settings panel for enabling/disabling the Reorder plugin for post types."
msgstr ""

#. Found in changelog list item.
msgid "Added advanced settings for overriding the menu order of post types."
msgstr ""

#. Found in changelog list item.
msgid "Added internationalization capabilities. "
msgstr ""

#. Found in changelog list item.
msgid "Slightly adjusted the styles of the Reordering interface."
msgstr ""

#. Found in changelog list item.
msgid "Updated 2014-12-11 - Ensuring WordPress 4.1 compatibility"
msgstr ""

#. Found in changelog list item.
msgid "Released 2013-07-19"
msgstr ""

#. Found in changelog list item.
msgid "Added new filter for editing the post-types supported"
msgstr ""

#. Found in changelog list item.
msgid "Thanks to mathielo for the suggestion and code contribution."
msgstr ""

#. Found in changelog list item.
msgid "Released 2012-08-09"
msgstr ""

#. Found in changelog list item.
msgid "Added expand/collapse section for nested post types"
msgstr ""

#. Found in changelog list item.
msgid "Added better page detection for scripts and styles"
msgstr ""

#. Found in changelog list item.
msgid "Released 2012-07-11"
msgstr ""

#. Found in changelog list item.
msgid "Added support for hierarchical post types"
msgstr ""

#. Found in changelog list item.
msgid "Released 2012-05-09"
msgstr ""

#. Found in changelog list item.
msgid "Updated screenshot"
msgstr ""

#. Found in changelog list item.
msgid "Corrected function prefix"
msgstr ""

#. Found in changelog list item.
msgid "Additional: changed readme.txt (didn't bump version number)"
msgstr ""

#. Found in changelog list item.
msgid "Added ability to post type of posts to be reordered"
msgstr ""

#. Found in changelog list item.
msgid "Fixed bug in initial order"
msgstr ""

#. Found in changelog list item.
msgid "Added ability to change menu name via class argument"
msgstr ""

#. Found in changelog list item.
msgid "Removed support for non-hierarchical post-types"
msgstr ""

#. Found in changelog list item.
msgid "Initial plugin release"
msgstr ""

#. Found in faq header.
msgid "Where's the settings page?"
msgstr ""

#. Found in faq header.
msgid "Where is the \"save\" button when re-ordering?"
msgstr ""

#. Found in faq header.
msgid "Do I need to add custom code to get this to work?"
msgstr ""

#. Found in faq header.
msgid "Can I use this on a single post type?"
msgstr ""

#. Found in faq header.
msgid "Does the plugin work with hierarchical post types?"
msgstr ""

#. Found in faq header.
msgid "Does it work in older versions of WordPress?"
msgstr ""

#. Found in faq header.
msgid "Does this sort posts within a category (i.e., a term)?"
msgstr ""

#. Found in faq paragraph.
msgid "The settings are located under Settings-&gt;Reorder Posts.  Settings are optional, of course, as the plugin will work with no configuration.  We consider the settings useful for only advanced users (i.e., users with coding experience)."
msgstr ""

#. Found in faq paragraph.
msgid "There isn't one. The changes are saved automatically."
msgstr ""

#. Found in faq paragraph.
msgid "Yes, and no.  There are many ways to retrieve posts using the WordPress API, and if the code has a <code>menu_order</code> sort property, the changes should be reflected immediately."
msgstr ""

#. Found in faq paragraph.
msgid "Often, however, there is no <code>menu_order</code> argument.  In the plugin's settings, there is an \"Advanced\" section which will attempt to override the <code>menu_order</code> property.  Please use this with caution."
msgstr ""

#. Found in faq paragraph.
msgid "You are able to override the post types used via a filter (see below) or navigate to the plugin's settings and enable which post types you would like to use."
msgstr ""

#. Found in faq paragraph.
msgid "Yes, but be wary that the plugin now allows you to re-nest hierarchical items easily."
msgstr ""

#. Found in faq paragraph.
msgid "This plugin requires WordPress 3.7 or above.  We urge you, however, to always use the latest version of WordPress."
msgstr ""

#. Found in faq paragraph.
msgid "No, but there is an add-on for this plugin called <a href=\"https://wordpress.org/plugins/reorder-by-term/\">Reorder by Term</a> you should check out."
msgstr ""