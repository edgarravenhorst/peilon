<?php

get_header(); ?>


<section  class="pagewrap">
	<header class="page-header">
		<h1 class="page-title">Pagina niet gevonden</h1>
	</header>
	<p>Wij hebben de opgevraagde pagina helaas niet kunnen vinden. Controleer of u de juiste invoer hebt gedaan.</p>
	<p>Klik <a href="<?php bloginfo('wpurl'); ?>">hier</a> om terug te keren naar de homepage</p>
</section><!-- .page-wrapper -->

<?php get_footer(); ?>
