<?php get_header(); ?>
<?php while ( have_posts() ) : the_post();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>
<section id="content">

	<section class="top" id='breadcrumbs'>
		<section class="pagewrap">
		<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
	yoast_breadcrumb();
}?>
			</section>
	</section>
	<?php if($image){ ?>
	<section id="banner" style="background-image:url('<?php echo $image[0]; ?>');">
		<section class="pagewrap">
			<h1 class="page_title"><?php the_title() ?></h1>
		</section>
	</section>
	<?php } ?>

	<section  class="pagewrap">
        <?php if (!$image) : ?>
        <h1 class="page_title"><?php the_title() ?></h1>
        <?php endif ?>

        <section id="col-right" <?php if($image){ ?>class="banner-space" <?php }?>>
            <section class="info">
                <h3>In het kort..</h3>
                <p><?php the_excerpt(); ?></p>

                <!-- <a href="#" class="more-info">Meer over samenwerken <i class="fa fa-arrow-right"></i></a> -->
            </section>
            <a href="<?php bloginfo('wpurl')?>/contact/stel-een-vraag/" class="btn">Zelf een vraag stellen <i class="fa fa-arrow-right"></i></a>
            <a href="<?php bloginfo('wpurl')?>/contact" class="btn">Contact opnemen <i class="fa fa-arrow-right"></i></a>
        </section>

        <article>
			<?php the_content(); ?>
		</article>


	</section>


</section>
<?php endwhile; ?>
<?php get_footer(); ?>
