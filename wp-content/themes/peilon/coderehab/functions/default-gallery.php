<?php

add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'show_thumbnails' => 0,
        'include' => '',
        'width' => 'auto',
        'height' => 'auto',
        'show_bullets' => 0,
        'float' => 0,
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

    // Here's your actual output, you may customize it to your need
    if ($float) $float = 'align' . $float;
    else $float = '';
    $output = "<section id='gallery_$post->ID' class='gallery $float' ><section class='sliderContainer'>";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
        // Fetch the thumbnail (or full image, it's up to you)
        // $img = wp_get_attachment_image_src($id, 'medium');
        // $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
        $img = wp_get_attachment_image_src($id, 'large');

        $output .= "<figure class='sliderElem'>";

        $output .= "<section class='imageholder'>";
        $output .= "<img class='slideimage' src='$img[0]' />";
        $output .= "</section>";

        $output .= "<a class='overlaybutton' href='" . get_permalink(get_the_ID()) . "'></a>";
        $output .= "</figure>";

    }

    $output .= "</section>";
    $output .= "<section class='buttons'><section class='btn-prev'></section><section class='btn-next'></section></section>";
    if($show_bullets) $output .=  "<div class='bullets'></div>";
    $output .= "</section>";

    $output .= "<script>
		var gallery_$post->ID
		jQuery(document).ready(function(){
			 gallery_$post->ID = new AllSlider('#gallery_$post->ID',  '$width' , '$height', {
                animType:'slide',
                hasSelector:$show_thumbnails,
                animVars:{
                    direction:'left',
                    descriptionAnim:'',
                    speed:7000,
                    animSpeed:900
                }
            });
		});
	</script>";

    wp_reset_query();
    return $output;
}
?>
