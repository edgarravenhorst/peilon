<?php

add_action( 'init', 'create_post_type_quotes' );
function create_post_type_quotes() {

    register_taxonomy(
        'tax_quotes',
        'quotes',
        array("hierarchical" => true,
              "label" => "Categorieën",
              //"singular_label" => "Vraagstuk",
              'update_count_callback' => '_update_post_term_count',
              'query_var' => true,
              //'rewrite' => array( 'slug' => 'quotes', 'with_front' => false ),
              'public' => true, // false, so no SEO box will be rendered
              'show_ui' => true,
              'show_tagcloud' => true,
              '_builtin' => false,
              'show_in_nav_menus' => true,
             )
    );

    register_post_type(
        'quotes',
        array(
            'labels' => array(
                'name' => __( 'Quotes' ),
                'singular_name' => __( 'Quote' ),
                'add_new' => 'Quote toevoegen'
            ),
            'public' => true, // false, so no SEO box will be rendered
            'has_archive' => true,

            'menu_position' => 5, // Onder berichten plaatsen
            'menu_icon'           => 'dashicons-format-quote',

            'rewrite' => array( 'slug' => 'quotes', 'with_front' => false ),
            'supports' => array(
              'title',
//              'editor',
//              'thumbnail',
//              'excerpt',
//              'page-attributes'
            ),
        )
    );
    flush_rewrite_rules();
}

//unregister SEO
function remove_yoast_metabox_reservations(){
    remove_meta_box('wpseo_meta', 'quotes', 'normal');
}
add_action( 'add_meta_boxes', 'remove_yoast_metabox_reservations',11 );

?>
