<?php

add_action( 'init', 'create_post_type_results' );
function create_post_type_results() {

  register_taxonomy('tax_results',
                    'results',
                    array("hierarchical" => true,
                          "label" => "Categorieën",
                          'update_count_callback' => '_update_post_term_count',
                          'query_var' => true,
                          'public' => true,'show_ui' => true,
                          'show_tagcloud' => true,
                          '_builtin' => false,
                          'show_in_nav_menus' => true
                         )
                   );

  register_post_type( 'results',
                     array(
                       'labels' => array(
                         'name' => __( 'Resultaten' ),
                         'singular_name' => __( 'Resultaat' ),
                         'add_new' => 'Resultaat toevoegen'
                       ),
                       'public' => true,
                       'has_archive' => true,
                       'rewrite' => array( 'slug' => 'resultaten', 'with_front' => false ),
                       'has_archive' => 'resultaten',
                       'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'tags'),
                       'taxonomies' => array('post_tag'),
                       'menu_icon'           => 'dashicons-screenoptions',

                     )
                    );
}

//add_filter('post_type_link', 'tax_results_permalink_structure', 10, 4);
//function tax_results_permalink_structure($post_link, $post, $leavename, $sample)
//{
//  if ( false !== strpos( $post_link, '%tax_results%' ) ) {
//    $event_type_term = get_the_terms( $post->ID, 'tax_results' );
//    $post_link = str_replace( '%tax_results%', array_pop( $event_type_term )->slug, $post_link );
//  }
//  return $post_link;
//}

flush_rewrite_rules();


?>
