<?php

add_action( 'init', 'create_post_type_projects' );
function create_post_type_projects() {


  register_post_type( 'project',
    array(
      'labels' => array(
        'name' => __( 'Projecten' ),
        'singular_name' => __( 'Project' ),
		'add_new' => 'Project toevoegen'
      ),
      'public' => true,
      'has_archive' => true,
		'rewrite' => array( 'slug' => 'project', 'with_front' => false ),
	  'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'),
      'taxonomies' => array('category'),
      'menu_icon'           => 'dashicons-cart',

    )
  );
	flush_rewrite_rules();
}
?>
