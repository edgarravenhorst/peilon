<?php

add_action( 'init', 'create_post_type_questions' );
function create_post_type_questions() {


	register_taxonomy('tax_question',
		'question',
		array("hierarchical" => true,
			"label" => "Categorien",
			//"singular_label" => "Vraagstuk",
			'update_count_callback' => '_update_post_term_count',
			'query_var' => true,
			'rewrite' => array( 'slug' => 'service', 'with_front' => false ),
			'public' => true,'show_ui' => true,
			'show_tagcloud' => true,
			'_builtin' => false,
			'show_in_nav_menus' => true
		)
	);



  register_post_type( 'question',
    array(
      'labels' => array(
        'name' => __( 'Vraagstukken' ),
        'singular_name' => __( 'Vraagstuk' ),
		'add_new' => 'Item toevoegen'
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
}

?>
