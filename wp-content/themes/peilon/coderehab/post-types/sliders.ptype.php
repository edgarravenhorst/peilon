<?php

add_action( 'init', 'create_slider_post_type' );
function create_slider_post_type() {


	register_taxonomy('tax_slider',
		'slider',
		array("hierarchical" => true,
			"label" => "Sliders",
			"singular_label" => "Sliders",
			'update_count_callback' => '_update_post_term_count',
			'query_var' => true,
			'rewrite' => array( 'slug' => 'service', 'with_front' => false ),
			'public' => true,'show_ui' => true,
			'show_tagcloud' => true,
			'_builtin' => false,
			'show_in_nav_menus' => true
		)
	);

	register_post_type( 'slider',
		array(
			'labels' => array(
				'name' => __( 'Sliders' ),
				'singular_name' => __( 'Sliders' ),
				'parent_item_colon' => '',
				'add_new' => 'Nieuw item'
			),
			'hierarchical' => true,
			'taxonomies' => array('tax_slider'),
			'show_in_nav_menus' => true,
			'map_meta_cap' => true,
			'public' => true,
			'has_archive' => true,
          'menu_icon'           => 'dashicons-format-gallery',
			'rewrite' => array( 'slug' => 'slider','with_front' => FALSE),
			'supports' => array('title','editor','thumbnail','excerpt','comments', 'page-attributes'),
		)
	);
}
?>
