<?php

add_action( 'init', 'create_post_type_steps' );
function create_post_type_steps() {



  register_post_type( 'steps',
    array(
      'labels' => array(
        'name' => __( 'Stappen' ),
        'singular_name' => __( 'Stap' ),
		'add_new' => 'Item toevoegen'
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
}

?>
