<?php

add_action( 'init', 'create_post_type_references' );
function create_post_type_references() {


  register_post_type( 'reference',
    array(
      'labels' => array(
        'name' => __( 'Referenties' ),
        'singular_name' => __( 'Referentie' ),
		'add_new' => 'Referentie toevoegen'
      ),
      'public' => true,
      'has_archive' => true,
		'rewrite' => array( 'slug' => 'referentie', 'with_front' => false ),
	  'supports' => array('title','editor','thumbnail','excerpt', 'page-attributes'),
    )
  );
	flush_rewrite_rules();
}

?>
