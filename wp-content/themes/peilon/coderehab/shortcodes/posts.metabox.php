<?php

add_action( 'add_meta_boxes', 'posts_properties_meta_box_add' );
add_action( 'save_post', 'posts_properties_meta_box_save' );

function posts_properties_meta_box_add()
{
    add_meta_box( 'posts');
}

function slide_properties_menu_box( $post )
{
    $values = get_post_custom( $post->ID );

    $heading = isset( $values['banner_heading'] ) ? esc_attr( $values['banner_heading'][0] ) : "";


?>
<div id='labels_props'>
    <p>
        <label for="banner_heading">Heading 1</label><br />
        <input type="text" name="banner_heading" id="banner_heading" value="<?php echo $heading; ?>" />
    </p>
</div>

<script>

    if (!jQuery('#show_slide_labels').prop('checked')) jQuery('#labels_props').hide();
    jQuery('#show_slide_labels').change(function(e){
        jQuery('#labels_props').toggle();
    });

</script>
<?php
}

function posts_properties_meta_box_save( $post_id )
{

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    if( isset( $_POST['banner_heading'] ) )
        update_post_meta( $post_id, 'banner_heading', $_POST['banner_heading']);

}

?>
