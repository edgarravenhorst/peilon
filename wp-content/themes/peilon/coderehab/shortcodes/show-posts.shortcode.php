<?php
function include_show_posts_func($atts) {
    extract(shortcode_atts(array(
        'count' => null,
        'category'
    ), $atts));


    $args = array('post_type'=> 'post', 'posts_per_page' => $count);

    $output = '<section class="shortcode-show-posts">';

    $loop = new WP_Query( $args );
    $first = ' first';
    while ( $loop->have_posts() ) {
        $loop->the_post();

        $output .= '<section id="post-' . get_the_id() . '" class="post' . $first . '">';
        if (has_featured_image())
            $output .= '<section class="featured-image" style="background-image:url(' . get_featured_image() . ')"></section>';

        $output .= '<h2 class="title"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h2>';
        $output .= '<section class="categories" >' . get_the_category_list() . '</section>';

        $output .= '<section class="postcontent">' . get_the_excerpt() . ' ';
        $output .= '<a href="' . get_the_permalink() . '" class="read-more">lees meer</a>';
        $output .= '</section></section>';

        $first = '';
    };

    $output .= '</section>';
    return $output;
}

add_shortcode('show-posts', 'include_show_posts_func');
