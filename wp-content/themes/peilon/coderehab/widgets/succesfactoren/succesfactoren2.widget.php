<?php

class succesfactorenWidget extends WP_Widget
{
  function succesfactorenWidget()
  {
    $widget_ops = array('classname' => 'succesfactoren-widget', 'description' => 'Toon succesfactoren' );
    $this->WP_Widget('succesfactorenWidget', 'Succesfactoren Widget', $widget_ops);
  }

  function form($instance)
  {
    $link_target = $instance['link_target'];
	include "widget-admin.view.php";
  }

  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['link_target'] = $new_instance['link_target'];

    return $instance;
  }

  function widget($args, $instance)
  {
  	add_action('wp_footer',array($this,'init_widget_scripts'));

    extract($args, EXTR_SKIP);

    echo $before_widget;
    $link_target = $instance['link_target'];

    include 'widget.view.php';
    echo $after_widget;
  }

  function init_widget_scripts(){
  }
}
add_action( 'widgets_init', create_function('', 'return register_widget("succesfactorenWidget");') );?>
