<div id='<?= $this->id ?>'>
  <p>
    <label for="<?php echo $this->get_field_id('list_item1'); ?>">Succesfactoren:
      <input class="widefat" id="<?php echo $this->get_field_id('list_item1'); ?>" name="<?php echo $this->get_field_name('list_item1'); ?>" type="text" value="<?php echo attribute_escape($list_item1); ?>" />
    </label>
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('link_target'); ?>"><?php _e('Linken naar:'); ?></label>
    <?php
    wp_dropdown_pages(
      array(
        'id' => $this->get_field_id('link_target'),
        'name' => $this->get_field_name('link_target'),
        'selected' => $link_target,
      )
    );
    ?>

  </p>
</div>
