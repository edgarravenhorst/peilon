<?php
add_action( 'widgets_init', 'widget_dynamic_fields' );
function widget_dynamic_fields() {
  register_widget( 'succesfactoren_lijst' );
}
class succesfactoren_lijst extends WP_Widget {
  function succesfactoren_lijst() {
    $widget_ops = array( 'classname' => 'coderehab', 'description' => __('Toon een lijst met verschillende succesfactoren', 'coderehab') );
    $control_ops = array( 'width' => 200, 'height' => 350, 'id_base' => 'coderehab-widget' );
    parent::__construct( 'coderehab-widget', __('Succesfactoren Lijst', 'coderehab'), $widget_ops, $control_ops );
  }
  function widget( $args, $instance ) {
    extract( $args );

    echo $before_widget;
    $link_target = $instance['link_target'];
?>


<section class="factors">
  <h3>Succesfactoren</h3>
  <ul>
    <?php
    for($i=0; $i<100; $i++){

      if (!empty($instance['dynamic-fields-'.$i])){
        echo '<li>' . $instance['dynamic-fields-'.$i] . '</li>';
      }
    }
    ?>
  </ul>
  <section class="right">
    <a href="<?php echo get_permalink($link_target) ?>" class="more-info">Dit wil ik ook</a> <i class="fa fa-arrow-right"></i></a>
  </section>
</section>

<?php
      echo $after_widget;
  }
  function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    for($i=0; $i<100; $i++){
      $instance['dynamic-fields-' . $i] = $new_instance['dynamic-fields-' . $i];
    }
    $instance['link_target'] = $new_instance['link_target'];
    return $instance;
  }
  function form( $instance ) {
    $link_target = $instance['link_target'];

    for($i= 0; $i<100; $i++){
      $defaults['dynamic-fields-' . $i] = '';
    }

    $instance = wp_parse_args( (array) $instance, $defaults );
    $widget_add_id = $this->id . "-add";
?>

<script>
  var $ =jQuery.noConflict();
  $(document).ready(function(e) {
    $(".<?php echo $widget_add_id; ?>" ).bind('click', function(e) {
      // unhide the following if there is empty value in it.:
      $.each($(".<?php echo $widget_add_id; ?>-input-containers").children(), function(){
        //console.log($(this).value());
        if($(this).val() == ''){
          $(this).show();
          return false; // bust out of the each loop if we showed a field
        }
      });
    });
  });
</script>
<p>
<div class="<?php echo $widget_add_id; ?>-input-containers">
  <!-- dynamic fields here -->
  <?php for( $i =0; $i<100; $i++){
      // here we hide the field if there is no value or it is empty
  ?>
  <input id="<?php echo $this->get_field_id( 'dynamic-fields-' . $i ); ?>"
         name="<?php echo $this->get_field_name( 'dynamic-fields-' . $i );?>"
         value="<?php echo $instance['dynamic-fields-' . $i]; ?>"
         class="widefat"
         <?php if(empty($instance['dynamic-fields-' . $i])){ echo 'style="display:none;"';  }else{echo 'style="margin: 5px 0;"';} ?>>

  <?php }
  ?>
  <div class="<?php echo $widget_add_id; ?> button button-primary" style="margin: 5px 0;">Voeg een succesfactor toe</div>
</div>
<p>
  <label for="<?php echo $this->get_field_id('link_target'); ?>"><?php _e('Linken naar:'); ?></label>
  <?php
    wp_dropdown_pages(
      array(
        'id' => $this->get_field_id('link_target'),
        'class' => 'widefat',
        'name' => $this->get_field_name('link_target'),
        'selected' => $link_target,
      )
    );
  ?>
</p>
<?php
  }
}
?>
