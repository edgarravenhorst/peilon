<?php
add_action( 'add_meta_boxes', 'succesfactoren_metabox' );

/* Do something with the data entered */
add_action( 'save_post', 'dynamic_save_postdata' );

/* Adds a box to the main column on the Post and Page edit screens */
function succesfactoren_metabox() {
  add_meta_box(
    'dynamic_sectionid',
    __( 'Succesfactoren', 'coderehab' ),
    'dynamic_inner_custom_box',
    'results');
}

/* Render the box content */
function dynamic_inner_custom_box() {
  global $post;
  // nonce for verification
  wp_nonce_field( plugin_basename( __FILE__ ), 'dynamicMeta_noncename' );
?>
<div id="meta_inner">
  <?php

  //Get the array of saved meta
  $succesfactoren = get_post_meta($post->ID,'succesfactoren',true);
  $linkto = get_post_meta($post->ID,'linkto',true);

  $c = 0;
  //if ( count( $succesfactoren ) > 0 ) {
  if( is_array ( $succesfactoren ) ){

    foreach( $succesfactoren as $succesfactor ) {

      if(isset($succesfactor['succesfactor'])){

        printf( '<p>Succesfactor <input class="widefat" style="margin-bottom:10px;" type="text" name="succesfactoren[%1$s][succesfactor]" value="%2$s" /><input class="button tagadd remove" type="button" value="%3$s"></p>', $c, $succesfactor['succesfactor'], __( 'Succesfactor verwijderen' ) );
        $c++;
      }
    }
  }

  ?>
  <span id="here"></span>
  <input class="button tagadd add button-primary" type="button" value="<?php _e('Succesfactor toevoegen'); ?>">
  <script>
    var $ =jQuery.noConflict();
    $(document).ready(function() {
      var count = <?php echo $c; ?>;
      $(".add").click(function() {
        count = count + 1;

        $('#here').append('<p>Succesfactor : <input class="widefat" style="margin-bottom:10px;" type="text" name="succesfactoren['+count+'][succesfactor]" value="" /><input class="button tagadd remove" type="button" value="<?php _e('Succesfactor verwijderen'); ?>">' );
        return false;
      });
      $(".remove").live('click', function() {
        $(this).parent().remove();
      });
    });
  </script>
  <p>
    <label for="linkto">Linken naar:</label>

    <?php
  wp_dropdown_pages(
    array(
      'id' => 'linkto',
      'class' => 'widefat',
      'name' => 'linkto',
      'selected' => $linkto,
    )
  );
?>
  </p>
</div>
<?php

}

/*  saves our custom data when the post is saved */
function dynamic_save_postdata( $post_id ) {
  // verify if this is an auto save routine.
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
    return;

  // verify Nonce and that the request is valid,
  if ( !isset( $_POST['dynamicMeta_noncename'] ) )
    return;

  if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename'], plugin_basename( __FILE__ ) ) )
    return;

  // GOOD; we are set, find a save data

  $succesfactoren = $_POST['succesfactoren'];
  $linkto = $_POST['linkto'];

  update_post_meta($post_id,'succesfactoren',$succesfactoren);
  update_post_meta( $post_id, 'linkto', $linkto);
}
?>
