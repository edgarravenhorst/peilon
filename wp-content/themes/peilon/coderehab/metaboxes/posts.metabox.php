<?php

add_action( 'add_meta_boxes', 'quotes_meta_box_add' );
add_action( 'save_post', 'quotes_meta_box_save' );

function quotes_meta_box_add()
{
    add_meta_box( 'head_properties-menu-select', 'Quote Inhoud', 'quotes_menu_box', 'quotes', 'normal', 'high' );
}

function quotes_menu_box( $post )
{
    $values = get_post_custom( $post->ID );

    $quote = isset( $values['quote'] ) ? esc_attr( $values['quote'][0] ) : "";
    $author = isset( $values['author'] ) ? esc_attr( $values['author'][0] ) : "";
    $about = isset( $values['about'] ) ? esc_attr( $values['about'][0] ) : "";


?>
<div id='labels_props'>
    <p>
        <label for="quote">Quote</label><br />
        <input type="text" name="quote" id="quote" style="width:100%" value="<?php echo $quote; ?>" />
    </p>
    <p>
        <label for="author">Auteur</label><br />
        <input type="text" name="author" id="author" style="width:100%" value="<?php echo $author; ?>" />
    </p>
    <p>
        <label for="about">Over auteur</label><br />
        <input type="text" name="about" id="about" style="width:100%" value="<?php echo $about; ?>" />
    </p>
</div>
<?php
}

function quotes_meta_box_save( $post_id )
{

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    if( isset( $_POST['quote'] ) )
        update_post_meta( $post_id, 'quote', $_POST['quote']);

      if( isset( $_POST['author'] ) )
        update_post_meta( $post_id, 'author', $_POST['author']);

      if( isset( $_POST['about'] ) )
        update_post_meta( $post_id, 'about', $_POST['about']);

}

?>
