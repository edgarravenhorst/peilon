<?php

add_action( 'add_meta_boxes', 'slide_properties_meta_box_add' );
add_action( 'save_post', 'slide_properties_meta_box_save' );

function slide_properties_meta_box_add()
{
    add_meta_box( 'slide_properties-menu-select', 'Slide options', 'slide_properties_menu_box', 'slider', 'normal', 'high' );
}

function slide_properties_menu_box( $post )
{
    $values = get_post_custom( $post->ID );

    $has_labels = isset( $values['show_slide_labels'] ) ? esc_attr( $values['show_slide_labels'][0] ) : "";
    $has_description = isset( $values['show_slide_description'] ) ? esc_attr( $values['show_slide_description'][0] ) : "";

    $heading1 = isset( $values['slide_heading1'] ) ? esc_attr( $values['slide_heading1'][0] ) : "";
    $heading1_2 = isset( $values['slide_heading1_2'] ) ? esc_attr( $values['slide_heading1_2'][0] ) : "";
    $heading2 = isset( $values['slide_heading2'] ) ? esc_attr( $values['slide_heading2'][0] ) : "";

	$textalign = isset( $values['slide_text_align'][0] ) ? $values['slide_text_align'][0]  : "left";

?>
<p>
    <input type="checkbox" name="show_slide_description" id="show_slide_description" <?php if ($has_description) echo 'checked'?>/><label for="show_slide_description">show description</label>
</p>
<p>
    <input type="checkbox" name="show_slide_labels" id="show_slide_labels" <?php if ($has_labels) echo 'checked'?>/><label for="show_slide_labels">show labels</label>
</p>

<div id='labels_props'>
    <p>
        <label for="slide_heading1">Heading 1</label><br />
        <input type="text" name="slide_heading1" id="slide_heading1" value="<?php echo $heading1; ?>" />
        <input type="text" name="slide_heading1_2" id="slide_heading1_2" value="<?php echo $heading1_2; ?>" />
    </p>
    <p>
        <label for="slide_heading2">Heading 2</label><br />
        <input type="text" name="slide_heading2" id="slide_heading2" value="<?php echo $heading2; ?>" />
    </p>
	<p>
		<label for="slide_text_align">Uitlijning</label><br/ >
		<select name="slide_text_align">

			<option value="center" <?php if($textalign == 'cenetr'){ echo 'selected'; }?>>Gecentreerd</option>

			<option value="top_left" <?php if($textalign == 'top_left'){ echo 'selected'; }?>>Links boven</option>
			<option value="top_center" <?php if($textalign == 'top_center'){ echo 'selected'; }?>>Midden boven</option>
			<option value="top_right" <?php if($textalign == 'top_right'){ echo 'selected'; }?>>Rechts boven</option>

			<option value="bottom_left" <?php if($textalign == 'bottom_left'){ echo 'selected'; }?>>Links onder</option>
			<option value="bottom_center" <?php if($textalign == 'bottom_center'){ echo 'selected'; }?>>Midden onder</option>
			<option value="bottom_right" <?php if($textalign == 'bottom_right'){ echo 'selected'; }?>>Rechts onder</option>
		</select>
	</p>
</div>

<script>

    if (!jQuery('#show_slide_labels').prop('checked')) jQuery('#labels_props').hide();
    jQuery('#show_slide_labels').change(function(e){
        jQuery('#labels_props').toggle();
    });

</script>
<?php
}

function slide_properties_meta_box_save( $post_id )
{

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    $has_labels = isset( $_POST['show_slide_labels'] )  ? 1 : 0;
    update_post_meta( $post_id, 'show_slide_labels', $has_labels );

    $has_description = isset( $_POST['show_slide_description'] ) ? 1 : 0;
    update_post_meta( $post_id, 'show_slide_description', $has_description );

    if( isset( $_POST['slide_heading1'] ) )
        update_post_meta( $post_id, 'slide_heading1', $_POST['slide_heading1']);

  if( isset( $_POST['slide_heading1_2'] ) )
        update_post_meta( $post_id, 'slide_heading1_2', $_POST['slide_heading1_2']);

    if( isset( $_POST['slide_heading2'] ) )
        update_post_meta( $post_id, 'slide_heading2', $_POST['slide_heading2']);

	 update_post_meta( $post_id, 'slide_text_align', $_POST['slide_text_align']);
}

?>
