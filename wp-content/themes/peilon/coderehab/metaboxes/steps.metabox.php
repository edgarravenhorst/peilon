<?php

add_action('add_meta_boxes','init_homepage_steps_meta_func');
add_action( 'save_post', 'homepage_steps_meta_box_save' );

function init_homepage_steps_meta_func(){

    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

    if ($template_file == 'pagetemplates/homepage.php') {
        add_meta_box("steps_meta", "Homepagina stappen", "homepage_steps_menu_box", "page", "normal", "low");
    }

}

function homepage_steps_menu_box( $post )
{
    $values = get_post_custom( $post->ID );

    $step1_title = isset( $values['step1_title'] ) ? esc_attr( $values['step1_title'][0] ) : "";
    $step1_content = isset( $values['step1_content'] ) ? esc_attr( $values['step1_content'][0] ) : "";
    $step1_link = isset( $values['step1_link'] ) ? esc_attr( $values['step1_link'][0] ) : "";

    $step2_title = isset( $values['step2_title'] ) ? esc_attr( $values['step2_title'][0] ) : "";
    $step2_content = isset( $values['step2_content'] ) ? esc_attr( $values['step2_content'][0] ) : "";
    $step2_link = isset( $values['step2_link'] ) ? esc_attr( $values['step2_link'][0] ) : "";

    $step3_title = isset( $values['step3_title'] ) ? esc_attr( $values['step3_title'][0] ) : "";
    $step3_content = isset( $values['step3_content'] ) ? esc_attr( $values['step3_content'][0] ) : "";
    $step3_link = isset( $values['step3_link'] ) ? esc_attr( $values['step3_link'][0] ) : "";

?>

<p>
    <label for="step1_title">Stap 1 Titel:</label><br />
    <input style="width:100%;" name="step1_title" id="step1_title" value="<?php echo $step1_title; ?>"/><br />
</p><p>
    <label for="step1_content">Stap 1 omschrijving:</label><br />
    <textarea style="width:100%; height:100px" name="step1_content" id="step1_content" ><?php echo $step1_content; ?></textarea>
</p><p>
    <label for="step1_link">Stap 1 Lees meer link:</label><br />
    <input style="width:100%;" name="step1_link" id="step1_link" value="<?php echo $step1_link; ?>"/><br />
</p>

<p>
    <label for="step2_title">Stap 2 Titel</label><br />
    <input style="width:100%;" name="step2_title" id="step2_title" value="<?php echo $step2_title; ?>"/><br />
</p><p>
    <label for="step2_content">Stap 2</label><br />
    <textarea style="width:100%; height:100px" name="step2_content" id="step2_content" ><?php echo $step2_content; ?></textarea>
</p><p>
<label for="step2_link">Stap 2 Lees meer link:</label><br />
<input style="width:100%;" name="step2_link" id="step2_link" value="<?php echo $step2_link; ?>"/><br />
</p>

<p>
    <label for="step3_title">Stap 3 Titel</label><br />
    <input style="width:100%;" name="step3_title" id="step3_title" value="<?php echo $step3_title; ?>"/><br />
</p><p>
    <label for="step3_content">Stap 3</label><br />
    <textarea style="width:100%; height:100px" name="step3_content" id="step3_content" ><?php echo $step3_content; ?></textarea>
</p><p>
<label for="step3_link">Stap 1 Lees meer link:</label><br />
<input style="width:100%;" name="step3_link" id="step3_link" value="<?php echo $step3_link; ?>"/><br />
</p>

<?php
}

function homepage_steps_meta_box_save( $post_id )
{

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    if( isset( $_POST['step1_title'] ) )
        update_post_meta( $post_id, 'step1_title', $_POST['step1_title']);
    if( isset( $_POST['step1_content'] ) )
        update_post_meta( $post_id, 'step1_content', $_POST['step1_content']);
    if( isset( $_POST['step1_link'] ) )
        update_post_meta( $post_id, 'step1_link', $_POST['step1_link']);

    if( isset( $_POST['step2_title'] ) )
        update_post_meta( $post_id, 'step2_title', $_POST['step2_title']);
    if( isset( $_POST['step2_content'] ) )
        update_post_meta( $post_id, 'step2_content', $_POST['step2_content']);
    if( isset( $_POST['step2_link'] ) )
        update_post_meta( $post_id, 'step2_link', $_POST['step2_link']);

    if( isset( $_POST['step3_title'] ) )
        update_post_meta( $post_id, 'step3_title', $_POST['step3_title']);
    if( isset( $_POST['step3_content'] ) )
        update_post_meta( $post_id, 'step3_content', $_POST['step3_content']);
    if( isset( $_POST['step3_link'] ) )
        update_post_meta( $post_id, 'step3_link', $_POST['step3_link']);

}



?>
