//Flickity slider
$(document).ready(function(){
  $('.quotes').flickity({
    // options
    cellAlign: 'left',
    contain: true,
   setGallerySize: false,
    pageDots: false,
    wrapAround: true,
    resize:true
  });
});

function onWindowResizeFunc() {
  // if (typeof(slider_home) !=='undefined') slider_home.onResizeHandler();
};


function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1);
    if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
  }
  return "";
}

function setCookie(cname, cvalue, exhours) {
  var d = new Date();
  d.setTime(d.getTime() + (exhours*60*60*1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires;
}

$(document).ready(function(){
  // if(typeof(slider_home) !=='undefined') slider_home.onResizeHandler();
  // window.onresize = onWindowResizeFunc;
  //button functions
  $('.toggle-top-menu').click(function(){
    $('nav#mobile-main-nav').toggleClass('active');
  });


  //------------- ANIMATION INTRO -----------------------//

  if(!getCookie('intro')){
    document.cookie="intro=false";
  }
  else{
    $("#intro-animation").hide();
  }


  // alleen afspelen op de homepage
  if($("body.page-template-homepage")[0]){

    if(getCookie('intro') == 'false'){

      // hide so that theres no scroll
      //$("#content, #slider").hide();

      // fade in logo
      $( "#intro-animation img.logo" ).delay(1000).fadeIn(700, function() {
        // fade out logo
        $( "#intro-animation img.logo" ).delay(1000).fadeOut(900);
        // show blockquote
        $( "#intro-animation blockquote" ).delay(1900).fadeIn(700, function() {
          $( "#intro-animation blockquote" ).delay(1000).fadeOut(900, function() {
            // $("#content, #slider").fadeIn();
            $("#intro-animation").fadeOut();
            setCookie('intro','true',3);
            slider_home.toSlide(0);
          });
        });
      });

    }



  }

  // MOBILE
  if(window.innerWidth < 1020){

    var active= false;

    $("#projects-overview .project-item").click(function(e){

      $("#projects-overview .project-item").removeClass('active');
      $(this).unbind('hover');

      if(active == true){
        $(this).removeClass('active');
        active = false;
      }

      else{

        $(this).addClass('active');
        active = true;
      }




    })

  }
  // DESKTOP
  if(window.innerWidth >760){

    $("#projects-overview .project-item").hover(function(e){
      $("#projects-overview .project-item.active").removeClass('active');
      $(this).unbind('hover');
      $(e.currentTarget).toggleClass('active');
    });

  }












  //------------- ANIMATION INTRO -----------------------//

});
