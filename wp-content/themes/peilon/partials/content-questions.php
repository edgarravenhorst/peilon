<section id="questions-overview">
    <section class="pagewrap">
        <h2 class="title page_title">Vraagstukken</h2>
        <?php

    $categories = get_categories( array('type' => 'post', 'number' => 6, 'exclude' => array(81)) );

        foreach($categories as $cat):

        $cat_url =  get_category_link($cat->cat_ID);
        $args = array('posts_per_page' => 4, 'category' => $cat->term_id, 'post_type' => 'post' );
        $posts = get_posts( $args );



        if (count($posts) > 0):?>
                <section class="questionbox">
					<h4><?php echo $cat->cat_name; ?></h4>
                    <ul>
                        <?php foreach($posts as $post): ?>

                        <li>
                            <a href="<?= get_permalink() ?>"><i class="fa fa-arrow-right"></i><?= get_the_title()?></a>
                        </li>

                        <?php endforeach; ?>
                    </ul>
                <button><a href="<?php echo $cat_url; ?>">Toon alles <i class="fa fa-arrow-right"></i></a></button>
            </section>
            <?php
        endif;
        endforeach;
            ?>
            <footer>
                <a href="<?php echo get_page_link(253) ?>" class="btn">Stel zelf een vraag! <i class="fa fa-arrow-right"></i></a>
                <h3>Heeft u zelf een vraag met betrekking tot een<br> van de bovenstaande onderwerpen?</h3>
            </footer>
    </section>

</section>
