<?php
    $args = array(
        'post_type' => "quotes"
    );

    $query = new WP_Query( $args );

    echo "<section class='quotes'>";
    while ( $query->have_posts() ) : $query->the_post();

    $quote = get_post_meta( get_the_ID(), 'quote', true );
    $author = get_post_meta( get_the_ID(), 'author', true );
    $about = get_post_meta( get_the_ID(), 'about', true );

    echo "<section class='quote'>";
    echo "<section class='pagewrap'>";
    echo "<blockquote>";
    echo $quote;
    echo "<span>";
    echo $author;
    echo "<br>";
    echo "<small>";
    echo $about;
    echo "</small>";
    echo "</span>";
    echo "</blockquote>";
    echo "</section>";
    echo "</section>";
    endwhile;

    echo "</section>";
    wp_reset_query();
?>

<!--
  <section id="quote">
    <section class="pagewrap">
      <blockquote>
        Expect your every need to be met.
        Expect the answer to every problem, expect abundance on every level. <br>
        <span>- Eileen Caddy -<br>
          <small>(Author and founder of the Findhorn Foundation)</small></span>
      </blockquote>
    </section>
</section>
-->
