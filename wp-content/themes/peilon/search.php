<?php
/*
Template Name: Search Page
*/
?>

<?php get_header();

global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
    $query_split = explode("=", $string);
    $search_query[$query_split[0]] = urldecode($query_split[1]);
} // foreach

$search = new WP_Query($search_query);
?>


<section id="content">
    <section id='breadcrumbs' class="top">
		<section class="pagewrap">
			<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
                yoast_breadcrumb();
            }?>
		</section>
	</section>
	<section class="pagewrap">

        <header id="search">
            <h1 class="title" class="">Zoeken</h1>
            <?php get_search_form(); ?>
        </header>

			<article>
                <?php if ($search_query['s'] != '' && $search->have_posts()) {

				while ( $search->have_posts() ) : $search->the_post(); ?>

                <section class="post hentry">
                    <h2><a href="<?= the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt();?>
                    <a href="<?php the_permalink();?>" class="btn">Lees verder <i class="fa fa-arrow-right"></i></a>
                </section>

				<?php endwhile; ?>

				<section class="pagination">
					<?php echo paginate_links(  ); ?>
				</section>
			<?php }
				else{
					echo 'Er zijn helaas geen resultaten gevonden, probeert u een andere zoekterm <br>of neem <a href="'. get_bloginfo('wpurl').'/contact">contact</a> met ons op.';
				}

			?>
		</article>

        <aside>
            <?php
                $sidebar = get_post_meta($post->ID, '_sidebar_name', true);
                $sidebar = ($sidebar != '')? $sidebar : 'page-sidebar';
                dynamic_sidebar($sidebar);
            ?>
        </aside>
	</section>
</section>



<?php get_footer(); ?>
