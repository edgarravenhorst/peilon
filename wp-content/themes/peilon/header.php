<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="author" content="code.rehab" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_url'); ?>/assets/fav-icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/assets/fav-icon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileImage" content="fav-icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#00a9b6">

    <title><?php wp_title(); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/site.css"  />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/flickity.css"  />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/site.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/external/jquery.flickity.min.js"></script>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class() ?> >

    <header id="main-header">
      <section class="pagewrap">
        <a href="<?php bloginfo('wpurl')?>" id="logo"><img src="<?php bloginfo('template_url'); ?>/assets/images/logo.jpg"/></a>
        <img src="<?php bloginfo('template_url'); ?>/assets/images/nav-toggle.png" id='showMenu' class='toggle-top-menu'/>

        <div class="mobile-font-resizer">
          <?php if(function_exists('fontResizer_place')) { fontResizer_place(); } ?>
        </div>
        <nav id="main-nav">
          <?php wp_nav_menu( array('theme_location' => 'main-menu') ); ?>

          <?php get_search_form(); ?>
          <?php if(function_exists('fontResizer_place')) { fontResizer_place(); } ?>
        </nav>
      </section>
    </header>

    <nav id="mobile-main-nav">
      <?php get_search_form(); ?>
      <?php wp_nav_menu( array('theme_location' => 'main-menu') ); ?>
    </nav>
