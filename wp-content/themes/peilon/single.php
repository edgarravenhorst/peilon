<?php get_header(); ?>
<?php while ( have_posts() ) : the_post();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>
<section id="content">

	<section class="top" id='breadcrumbs'>
		<section class="pagewrap">
			<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
	yoast_breadcrumb();
}?>
		</section>
	</section>

	<?php if($image){ ?>
	<section id="banner" style="background-image:url('<?php echo $image[0]; ?>');">
		<section class="pagewrap">
			<h1 class="page_title"><?php echo array_shift(get_post_meta($post->ID,'banner_heading'))?></h1>
		</section>
	</section>
	<?php } ?>

	<section  class="pagewrap">
		<?php if (!$image) : ?>
		<h1><?php the_title() ?></h1>
		<?php endif ?>
		<section id="col-right" <?php if($image){ ?>class="banner-space" <?php }?>>
			<section class="info">
				<h3>In het kort..</h3>
				<p><?php the_excerpt(); ?></p>

				<!-- <a href="#" class="more-info">Meer over samenwerken <i class="fa fa-arrow-right"></i></a> -->
			</section>
			<a href="<?php bloginfo('wpurl')?>/contact/stel-een-vraag/" class="btn">Zelf een vraag stellen <i class="fa fa-arrow-right"></i></a>
			<a href="<?php bloginfo('wpurl')?>/contact" class="btn">Contact opnemen <i class="fa fa-arrow-right"></i></a>


			<!-- Gerelateerde vraagstukken -->


			<section class="related">
				<h2>Gerelateerde vraagstukken</h2>
				<ul class="arrows">
					<?php
			   $tags = wp_get_post_tags(get_the_ID());

			   if ($tags) {
				   $tag_ids = array();
				   foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
				   $args=array(
					   'tag__in' => $tag_ids,
					   'post__not_in' => array($post->ID),
					   'posts_per_page'=>5, // Number of related posts to display.
					   'caller_get_posts'=>1
				   );

				   $my_query = new wp_query( $args );

				   while( $my_query->have_posts() ) {
					   $my_query->the_post();

					   echo '<li><a href="'.get_the_permalink().'"><i class="fa fa-arrow-right"></i>'. get_the_title() . '</a></li>';
				   }
			   }
			   $post = $orig_post;
			   wp_reset_query();
			?>
				</ul>

				<a class="all-questions-link" href="<?php bloginfo('wpurl') ?>/vraagstukken/">Toon alles <i class="fa fa-arrow-right"></i></a>
			</section>

			<!-- einde gerelateerde vraagstukken -->

		</section>

		<article>
			<?php the_content(); ?>
		</article>
	</section>


</section>
<?php endwhile; ?>
<?php get_footer(); ?>
