<?php

//display errors
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

//INCLUDES
//=======================================================================

require_once "coderehab/functions/featured-image.php";
//require_once "coderehab/functions/default-gallery.php";
require_once "coderehab/functions/mobile-detect.php";
require_once "coderehab/metaboxes/slider.metabox.php";
require_once "coderehab/metaboxes/steps.metabox.php";
require_once "coderehab/metaboxes/posts.metabox.php";
require_once "coderehab/metaboxes/succesfactoren.metabox.php";

require_once "coderehab/post-types/sliders.ptype.php";
require_once "coderehab/post-types/results.ptype.php";
//require_once "coderehab/post-types/projects.ptype.php";
//require_once "coderehab/post-types/references.ptype.php";
require_once "coderehab/post-types/quotes.ptype.php";

require_once "coderehab/shortcodes/cr-post-slider.shortcode.php";
require_once "coderehab/shortcodes/show-posts.shortcode.php";

//require_once "coderehab/widgets/succesfactoren/succesfactoren.widget.php";

//SETTINGS, ACTIONS, FILTERS
//=======================================================================

//settings
add_theme_support(
    'post-thumbnails', array(
        'post',
        'events',
        'page',
        'slider',
        'partner',
		'project',
      'reference',
      'results'
    )
);

// add_image_size('single', 1200, 300, true);

//actions
add_action( 'widgets_init', 'register_default_sidebars' );
add_action( 'after_setup_theme', 'register_default_menus' );
add_action( 'init', 'my_theme_add_editor_styles' );

//filters
add_filter('mce_buttons_2', 'add_editor_styleselect_btn');
add_filter( 'tiny_mce_before_init', 'setup_custom_editor_settings' );
add_filter('wp_mail_from', 'set_mail_from');
add_filter('wp_mail_from_name', 'set_mail_from_name');

add_action( 'init', 'my_add_excerpts_to_pages' );

// functions
//=======================================================================

function my_theme_add_editor_styles() {
    add_editor_style( 'assets/css/wp-editor-style.css' );
    add_editor_style( 'assets/css/font-awesome.min.css' );
}

function my_add_excerpts_to_pages() {
    add_post_type_support( 'page', 'excerpt' );
}

//action: widgets_init
function register_default_sidebars() {

    register_sidebar( array(
        'name'          => __( 'Sidebar', 'default-sidebar' ),
        'id'            => 'page-sidebar',
        'before_widget' => '<div class="sidebar-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));

    register_sidebar( array(
        'name'          => __( 'Single ProjectSidebar', 'single-project-sidebar' ),
        'id'            => 'single-project-sidebar',
        'before_widget' => '<div class="sidebar-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));

	register_sidebar( array(
        'name'          => __( 'Footer', 'footer-widgets' ),
        'id'            => 'footer-widgets',
        'before_widget' => '<div class="sidebar-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ));

	register_sidebar( array(
        'name'          => __( 'Contact', 'contact-sidebar' ),
        'id'            => 'contact-sidebar',
        'before_widget' => '<div class="sidebar-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ));
}

function register_default_menus() {
    register_nav_menu( 'main-menu', 'Main-menu' );
}

// filter: tiny_mce_before_init
function setEditorColors( $init ) {

}

// filter: wp_mail_from
function set_mail_from($old) {
    return get_option('admin_email');
}

// filter: wp_mail_from_name
function set_mail_from_name($old) {
    return get_option('blogname');
}

//filter:mce_buttons_2
function add_editor_styleselect_btn( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

//filter:tiny_mce_before_init
function setup_custom_editor_settings( $init_array ) {

    // Define the style_formats array
    $style_formats = array(
        array(
            'title' => 'Button',
            'inline' => 'a',
            'classes' => 'button',
            'wrapper' => false,

        ),

        array(
            'title' => 'Button Dark',
            'inline' => 'a',
            'classes' => 'button dark',
            'wrapper' => false,

        ),

        array(
            'title' => 'Button-full-width',
            'inline' => 'a',
            'classes' => 'button full-width',
            'wrapper' => false,
        ),
    );

    //change default colors
    $default_colours = '[
	    "000000", "Black",
	    "ffffff", "White",
	    "ababab", "Gray",
	    "448993", "green1",
	    "009ba7", "green2",
	    "5d6e7f", "gray",
	    "004f64", "Blue",
	]';

    $init_array['textcolor_map'] = $default_colours;
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
}
