<?php

/*
Template Name: Home
*/
get_header(); ?>

<section id="slider">
    <?= do_shortcode('[rehabslider id="slider_home" height="auto" class="homepage-slider" post_type="slider" custom_tax="tax_slider" category_name="homeslider"]') ?>
</section>

<section id="content">
	<span id="werkwijze"></span>

    <section class="pagewrap">
        <article class="intro">
            <?php while ( have_posts() ) : the_post(); ?>
            <h2><?php the_title(); ?></h2>
            <?php the_content(); ?>
            <?php endwhile; ?>
        </article>
    </section>

    <section id="steps" class="pagewrap">
        <span class='row'>
        <section class="col">
            <section>
                <h2><?= get_post_meta( get_the_ID(), 'step1_title', true ); ?></h2>
                <p><?= get_post_meta( get_the_ID(), 'step1_content', true ); ?></p>
                <a href="<?= get_post_meta( get_the_ID(), 'step1_link', true ); ?>">Meer over stap 1 <i class="fa fa-arrow-right"></i></a>
            </section>
        </section>

        <section class="col">
            <section>
                <h2><?= get_post_meta( get_the_ID(), 'step2_title', true ); ?></h2>
                <p><?= get_post_meta( get_the_ID(), 'step2_content', true ); ?></p>
                <a href="<?= get_post_meta( get_the_ID(), 'step2_link', true ); ?>">Meer over stap 2 <i class="fa fa-arrow-right"></i></a>
            </section>
        </section>

        <section class="col">
            <section>
                <h2><?= get_post_meta( get_the_ID(), 'step3_title', true ); ?></h2>
                <p><?= get_post_meta( get_the_ID(), 'step3_content', true ); ?></p>
                <a href="<?= get_post_meta( get_the_ID(), 'step3_link', true ); ?>">Meer over stap 3 <i class="fa fa-arrow-right"></i></a>
            </section>
        </section>
        </span>
    </section>

    <?php get_template_part( 'partials/content', 'quotes' ); ?>
    <?php get_template_part( 'partials/content', 'questions' ); ?>

    <header id="about-us-header">
        <section class="pagewrap">
            <h2>Over Peilon</h2>
        </section>
    </header>
    <section id="about-us">
        <section class="pagewrap">
            <section class="col">
                <img src="<?php bloginfo('template_url'); ?>/assets/images/erikvandenboogaard.jpg" />
                <a href="<?php echo get_page_link(17) ?>" class="btn">Contact opnemen <i class="fa fa-arrow-right"></i></a>
				<a target="_blank" href="https://nl.linkedin.com/pub/erik-van-den-boogaard/4/716/394" class="btn">Bekijk mijn Linkedin profiel<i class="fa fa-linkedin"></i></a>
            </section>
            <article class="col">
                <?php
                $post = get_post(43);
                @setup_postdata( $post, $more_link_text, $stripteaser );
                echo '<h2>'.get_the_title().'</h2>';
                the_content();
                wp_reset_postdata();
                ?>
            </article>
            <article class="col">
                <?php
                $post = get_post(47);
                @setup_postdata( $post, $more_link_text, $stripteaser );
                echo '<h2>'.get_the_title().'</h2>';
                the_content();
                wp_reset_postdata();
                ?>
            </article>
        </section>
    </section>
</section>

	<section id="intro-animation">
			<img class="logo" src="<?php bloginfo('template_url'); ?>/assets/images/intro-logo.jpg"/>
			<blockquote> <span>Peilon</span>,<br class="mobile"/> doet wat werkt!</blockquote>
		</section>

<?php get_footer(); ?>

