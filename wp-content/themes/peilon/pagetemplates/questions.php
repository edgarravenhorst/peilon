<?php

/*
Template Name: Vraagstukken-overzicht
*/
get_header();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>

<section id="content">

	<!-- Breadcrumb here -->
	<section class="top" id='breadcrumbs'>
		<section class="pagewrap">
		<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
	yoast_breadcrumb();
}?>
	</section>
	</section>
	<!-- End breadcrumb -->

	<?php if($image){ ?>
	<section id="banner" style="background-image:url('<?php echo $image[0]; ?>');">
		<section class="pagewrap">
			<h1><?php the_title() ?></h1>
		</section>
	</section>
	<?php } ?>


	<section class="pagewrap">
		<article>
			<?php while ( have_posts() ) : the_post();
			the_content();
			endwhile
			?>
		</article>

		<section id="questions-overview">

		<?php
		//list terms in a given taxonomy (useful as a widget for twentyten)
		$args = array('type' => 'post');
          $categories = get_categories( $args );

			foreach($categories as $cat){
              if($cat->slug !== 'vraagstukken'){

				$cat_url =  get_category_link($cat->cat_ID);

				echo '<section class="questionbox">
							<h4>'.$cat->cat_name.'</h4>
							<ul>';

				global $post;
			$args = array( 'numberposts' => 4, 'category' => $cat->term_id );
			$posts = get_posts( $args );

				foreach($posts as $post){

					echo '<li><a href="'.get_permalink().'"><i class="fa fa-arrow-right"></i>'.get_the_title().'</li></a>';

				}


	echo '</ul>
				<button><a href="'.$cat_url.'">Toon alles <i class="fa fa-arrow-right"></i></a></button>
			</section>';
            }
}
?>
		</section>

	</section>

	</section>



</section>

<?php get_footer(); ?>

