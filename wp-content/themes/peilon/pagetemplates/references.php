<?php
/*
Template Name: Referencies-overzicht
*/

get_header();
$categories = get_the_category();
?>

<section id="content">

	<section class="top" id='breadcrumbs'>
		<section class="pagewrap">
			<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
	yoast_breadcrumb(); }?>
		</section>
	</section>

	<?php while ( have_posts() ) : the_post(); ?>

	<section  class="pagewrap">
		<h1 class="page_title"><span class='grey'>Referenties</span></h1>
	<section id="col-right">
		<section class="info">
			<h3>In het kort..</h3>
			<p><?php the_excerpt(); ?></p>
		</section>
		<a href="<?php echo get_page_link(253) ?>" class="btn">Zelf een vraag stellen <i class="fa fa-arrow-right"></i></a>
		<a href="<?php echo get_page_link(17) ?>" class="btn">Contact opnemen <i class="fa fa-arrow-right"></i></a>
	</section>

	<article>


		<?php
			the_content();

			endwhile;



		$args = array( 'post_type' => 'reference', 'posts_per_page' => 18 );
			$loop = new WP_Query( $args );
			$counter = 0;

			while ( $loop->have_posts() ) : $loop->the_post(); ?>

		<section class="post withImg">
			<?php the_post_thumbnail('thumbnail'); ?>
			<h2><?php echo get_the_title() ?></h2>
			<p><?php
	$content = get_the_excerpt();
				echo substr(strip_tags($content),0,350) . '...';
				?>

			</p>
		</section>



		<?php endwhile ?>


	<section class="pagination">
		<?php

	global $wp_query;
		$big = 999999999; // need an unlikely integer

		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages,
			'prev_text' => __('Nieuwere vraagstukken'),
			'next_text' => __('Oudere vraagstukken'),
			'before_page_number' => '<span class="pagenumber-wrapper">',
	'after_page_number'  => '</span>'
		) );
		?>

	</section>
	</article>
</section>
</section>

<?php get_footer(); ?>
