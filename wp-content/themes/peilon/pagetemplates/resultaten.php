<?php

/*
Template Name: Resultaten-blokken-overzicht
*/
get_header();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>

<section id="content">
  <!-- Breadcrumb here -->
  <section class="top" id='breadcrumbs'>
    <section class="pagewrap">
      <?php if ( function_exists( 'yoast_breadcrumb' ) ) {
  yoast_breadcrumb();
}?>
    </section>
  </section>
  <!-- End breadcrumb -->


  <section class="pagewrap">
    <article>
      <?php while ( have_posts() ) : the_post(); ?>
      <h1><?php the_title(); ?></h1>

      <?php
      the_content();
      endwhile
      ?>
    </article>

    <section id="projects-overview">

      <?php
        $args = array( 'post_type' => 'results', 'posts_per_page' => 18 );
      $loop = new WP_Query( $args );
      $counter = 0;
      while ( $loop->have_posts() ) : $loop->the_post();
      $counter++;

      $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );

      ?>


      <div class="project-item">
        <span class="close">X</span>
        <span class="shadow"></span>
        <span class="overlay"></span>
        <span class="image" style="background-image:url('<?php echo $feat_image; ?>')"></span>
        <span class="content">
          <h3><?php the_title(); ?></h3>
          <h4><?php echo get_the_category_list(","); ?></h4>
          <p><?php the_subtitle(); ?></p>
          <a class="btn" href="<?php the_permalink(); ?>">Meer informatie</a>
        </span>
      </div>

      <?php endwhile; ?>
    </section>

  </section>

</section>

</section>

<?php get_footer(); ?>

