<?php
/*
Template Name: Contact
*/
get_header(); ?>

<section id="content">

	<!-- Breadcrumb here -->
	<section class="top" id='breadcrumbs'>
		<section class="pagewrap">
			<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
	yoast_breadcrumb();
}?>
		</section>
	</section>

	<section  class="pagewrap">
		<article>
			<?php
while ( have_posts() ) : the_post();
the_content();
endwhile
			?>
		</article>

		<section id="col-right">
			<?php
                $sidebar = get_post_meta($post->ID, '_sidebar_name', true);
                $sidebar = ($sidebar != '')? $sidebar : 'contact-sidebar';
                dynamic_sidebar($sidebar);
            ?>
		</section>

	</section>




</section>

<!--
<section id="google-maps">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2447.9221379775277!2d6.401851999999999!3d52.153925699999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c78cf23aab13e9%3A0xcc37777a9f3644df!2sSchweitzerweg+43%2C+7242+HP+Lochem!5e0!3m2!1snl!2snl!4v1441217496238" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</section> -->

<?php get_footer(); ?>
