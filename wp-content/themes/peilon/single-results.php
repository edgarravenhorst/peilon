<?php get_header(); ?>
<?php while ( have_posts() ) : the_post();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$category = get_the_category();
$category = get_the_terms($post->id, 'tax_results');

?>

<section id="content">

  <section class="top" id='breadcrumbs'>
    <section class="pagewrap">
      <?php if ( function_exists( 'yoast_breadcrumb' ) ) {
  yoast_breadcrumb();
}?>
    </section>
  </section>

  <section  class="pagewrap">

    <article>
      <h1><?php echo $category[0]->name ?></h1>
      <h2>Klant: <?php the_title() ?></h2>
      <!--      <h3><span>Ons werk: </span><?php echo get_the_excerpt(); ?></h3>-->
      <h3><span>Ons werk: </span><?php echo get_the_subtitle(); ?></h3>
      <?php the_content(); ?>

      <section id="pagination">
        <!--<?php previous_post_link('%link','&lt;', TRUE); ?>-->
        <!--<?php var_dump(previous_post_link()); ?>-->
        <?php if( get_previous_post() ) : ?>
        <div class="nav-prev"><?php previous_post_link('%link','<i class="fa fa-arrow-left"></i>Vorige project') ?></div>
        <?php endif; ?>

        <?php $url =  get_home_url().'/resultaten-overzicht' ?>
        <span class="center">
          <a href="<?php echo $url; ?>">
            <i class="fa fa-th-large"></i><br>
            Terug naar overzicht
          </a>
        </span>
        <!--<?php next_post_link('%link','&gt;', TRUE); ?>-->

        <?php if( get_next_post() ) : ?>
        <div class="nav-next"><?php next_post_link('%link','Volgend project<i class="fa fa-arrow-right"></i>') ?></div>
        <?php endif; ?>
      </section>
    </article>

        <section id="col-right" class="banner-space">
      <!--      <?php dynamic_sidebar( 'single-project-sidebar' ); ?>-->

      <?php
      $succesfactoren = get_post_meta($post->ID,'succesfactoren',true);
      $linkto = get_post_meta($post->ID,'linkto',true);
      ?>

      <section class="factors">
        <h3>Succesfactoren:</h3>
        <ul>
          <?php
          foreach( $succesfactoren as $succesfactor ) {
            if(isset($succesfactor['succesfactor'])){
              printf( '<li>%2$s</li>', $c, $succesfactor['succesfactor']);
              $c++;
            }
          }
          ?>
        </ul>
        <section class="right">
          <a class="more-info" href="<?php echo get_permalink($linkto) ?>">Dit wil ik ook <i class="fa fa-arrow-right"></i></a>
        </section>
      </section>

      <!-- op basis van meta-->
    </section>
  </section>


</section>
<?php endwhile; ?>
<?php get_footer(); ?>
